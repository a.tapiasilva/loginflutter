import 'package:flutter/material.dart';
import 'package:validate/validate.dart';

void main() => runApp(new MaterialApp(
      title: 'Login Espacio Seguro',
      home: new LoginPage(),
    ));

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

class _LoginData {
  String email = '';
  String password = '';
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  _LoginData _data = new _LoginData();

  String _validateEmail(String value) {
    // If empty value, the isEmail function throw a error.
    // So I changed this function with try and catch.
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'Debe ingresar un RUT válido.';
    }

    return null;
  }

  String _validatePassword(String value) {
    if (value.length < 6) {
      return 'La contraseña debe ser al menos de 6 caracteres..';
    }

    return null;
  }

  void submit() {
    // First validate form.
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save(); // Save our form now.

      print('Printing the login data.');
      print('Email: ${_data.email}');
      print('Password: ${_data.password}');
    }
  }

  @override
  //Colores
  final morado = const Color(0xFF913a90);
  final naranjo = const Color(0xFFf15a24);
  final gris = const Color(0xFF525252);
  final double textSize = 40.0;

  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Espacio Seguro'),
          backgroundColor: morado,
        ),
        body: new ListView(
          children: <Widget>[
            new Image.asset(
              'images/logo_login.png',
            width: 80.0,
            height: 100.0, 
            ),
            new Container(
                padding: new EdgeInsets.all(20.0),
                child: new Form(
                  key: this._formKey,
                  child: new ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      new TextFormField(
                          keyboardType: TextInputType
                              .emailAddress, // Use email input type for emails.
                          decoration: new InputDecoration(
                            hintText: 'Ejemplo: 18.604.258-1',
                            labelText: 'Ingresar RUT',
                            icon: Icon(Icons.account_circle),
                          ),
                          validator: this._validateEmail,
                          onSaved: (String value) {
                            this._data.email = value;
                          }),
                      new TextFormField(
                          obscureText: true, // Use secure text for passwords.
                          decoration: new InputDecoration(
                              hintText: 'Ingresar contraseña',
                              labelText: 'Ingresar Contraseña',
                              icon: Icon(Icons.lock)),
                          validator: this._validatePassword,
                          onSaved: (String value) {
                            this._data.password = value;
                          }),
                      new Container(
                        width: screenSize.width,
                        child: new RaisedButton(
                          child: new Text(
                            'Ingresar',
                            style: new TextStyle(color: Colors.white),
                          ),
                          onPressed: this.submit,
                          color: naranjo,
                        ),
                        margin: new EdgeInsets.only(top: 20.0),
                      )
                    ],
                  ),
                )),
          ],
        ));
  }
}
